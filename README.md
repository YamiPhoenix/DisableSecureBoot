Copyright 2018 Eliza Loper
Licensed GPLv3.0 or higher

This application creates a USB key capable of disabling SecureBoot on a headless Dell 300x series LoT gateway.

